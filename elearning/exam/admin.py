from __future__ import unicode_literals

from django.contrib import admin
from exam import models

class SoalAdmin(admin.ModelAdmin):
  list_filter = ('kode_ujian',)
  search_fields = ('kode_ujian',)

admin.site.site_header = "ONLINE EXAM Administrator"


# Register your models here.
admin.site.register(models.MataKuliah)
admin.site.register(models.Ujian)
admin.site.register(models.Soal, SoalAdmin)
admin.site.register(models.Student)
admin.site.register(models.Answer)
admin.site.register(models.Nilai)
