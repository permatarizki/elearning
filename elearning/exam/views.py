from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView, CreateView, ListView, DetailView, DeleteView, UpdateView
from exam.models import Soal, Student, Answer
from django.core.exceptions import ObjectDoesNotExist

# Create your views here.
class ResultExamView(TemplateView):
    template_name = "exam/result.html"
    def post(self, request, *args, **kwargs):
        kode_ujian = request.POST['kode_ujian']
        nomor_induk = request.POST['nomor_induk']
        #TODO matching answer vs soal
        #get soal
        soals = Soal.objects.all().filter(kode_ujian__kode_kuis=kode_ujian)
        total_soal = soals.count()
        count_benar = 0
        count_salah = 0
        for soal in soals:
            temp_ans = Answer.objects.all().filter(soal__kode_ujian__kode_kuis=kode_ujian, soal__nomor=soal.nomor, student__student_id = nomor_induk).first()
            if temp_ans != None:
                if soal.jawaban_benar != 'Not Multiple choice':
                    if soal.jawaban_benar == temp_ans.jawaban:
                        print(soal.jawaban_benar)
                        print(temp_ans.jawaban)
                        count_benar = count_benar + 1
                    else:
                        count_salah = count_salah + 1
            else:
                return render(request, 'exam/empty.html', {
                    'msg': 'Tidak ada hasil untuk nomor siswa: '+nomor_induk,
                })

        ans = Answer.objects.all().filter(soal__kode_ujian__kode_kuis=kode_ujian, student__student_id = nomor_induk)
        if ans.count() == 0:
            return render(request, 'exam/empty.html', {
                'msg': 'Tidak ada hasil untuk kode ujian '+kode_ujian,
            })
        else:
            return render(request, 'exam/result_list.html', {
                'answers': ans,
                'benar':count_benar,
                'salah':count_salah,
                'nomor_induk':nomor_induk,
                'score':(count_benar/total_soal)*100,
            })

class StartExamView(TemplateView):
    template_name = "exam/start.html"

    def post(self, request, *args, **kwargs):
        kode_ujian = request.POST['kode_ujian']
        nama_lengkap = request.POST['nama_lengkap']
        nomor_induk = request.POST['nomor_induk']
        nomor_soal = request.POST['nomor_soal']
        answer = request.POST.get('answer', '');#return empty string if not available
        temp = int(nomor_soal)

        #Initiate student name & number
        if temp == 0 :
            try:
                user  = Student.objects.get(student_id=nomor_induk)
            except ObjectDoesNotExist:
                user = None

            if user == None:
                user = Student.objects.create(name=nama_lengkap, student_id=nomor_induk, prodi='SE')
                user.save()
        else:
            #student, soal, jawaban
            #get soal
            user  = Student.objects.get(student_id=nomor_induk)
            soal = Soal.objects.get(kode_ujian__kode_kuis=kode_ujian, nomor= int(nomor_soal))
            obj = Answer.objects.create(student= user, soal=soal, jawaban=answer)
            obj.save()

        questions = Soal.objects.all().filter(kode_ujian__kode_kuis=kode_ujian, nomor= int(nomor_soal)+1)
        questions_length = Soal.objects.all().filter(kode_ujian__kode_kuis=kode_ujian).count()
        # print(temp)
        # print(questions_length)
        if questions_length == 0:
            return render(request, 'exam/invalid_code.html', {
                'message': 'Ujian sudah selesai',
            })
        if temp < questions_length :
            return render(request, 'exam/question.html', {
                'kode_ujian':kode_ujian,
                'nama_lengkap': nama_lengkap,
                'nomor_induk': nomor_induk,
                'questions': questions,
            })
        else :
            return render(request, 'exam/end.html', {
                'message': 'Ujian sudah selesai',
            })
