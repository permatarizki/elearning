from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^start/',views.StartExamView.as_view(),name='start'),
    url(r'^result/',views.ResultExamView.as_view(),name='result'),
]
