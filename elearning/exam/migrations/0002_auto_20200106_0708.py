# Generated by Django 2.2 on 2020-01-06 07:08

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('exam', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='answer',
            name='student_id',
        ),
        migrations.RemoveField(
            model_name='answer',
            name='student_name',
        ),
        migrations.AddField(
            model_name='answer',
            name='student',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, related_name='student', to='exam.Student'),
            preserve_default=False,
        ),
    ]
