from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

# Create your models here.

class MataKuliah(models.Model):
    deskripsi = models.CharField(max_length=200)
    kode = models.CharField(max_length=10)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '{}-{}'.format(self.kode, self.deskripsi)

class Ujian(models.Model):
    mata_kuliah = models.ForeignKey('MataKuliah', on_delete=models.CASCADE,related_name='matkul')
    kode_kuis = models.CharField(max_length=20)

    def __str__(self):
        return '{}-{}'.format(self.mata_kuliah, self.kode_kuis)

class Student(models.Model):
    created_date = models.DateTimeField(default=timezone.now)
    name = models.CharField(max_length=100)
    student_id =  models.CharField(max_length=10, unique=True)
    prodi =  models.CharField(max_length=50, default='')
    def __str__(self):
        return '{}'.format(self.name)

class Soal(models.Model):
    kode_ujian = models.ForeignKey('Ujian', on_delete=models.CASCADE,related_name='ujian')
    created_date = models.DateTimeField(default=timezone.now)
    nomor = models.IntegerField(blank=True, null=True)
    pertanyaan = models.TextField()
    A = models.CharField(max_length=200, default='')
    B = models.CharField(max_length=200, default='')
    C = models.CharField(max_length=200, default='')
    D = models.CharField(max_length=200, default='')
    E = models.CharField(max_length=200, default='')
    OPSI_JAWABAN = (
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D'),
        ('E', 'E'),
        ('Not Multiple choice', 'Not Multiple choice'),
    )
    jawaban_benar = models.CharField(max_length=50,choices=OPSI_JAWABAN,blank=True, null=True)
    image = models.FileField(upload_to='media/elearning/img', blank=True, null=True)
    file = models.FileField(upload_to='media/elearning/file', blank=True, null=True)
    durasi_minute = models.IntegerField(default = 3)
    durasi_second = models.IntegerField(default = 0)
    def __str__(self):
        return '{}-{}'.format(self.kode_ujian, self.nomor)

class Answer(models.Model):
    student = models.ForeignKey('Student', on_delete=models.CASCADE,related_name='student')
    soal = models.ForeignKey('Soal', on_delete=models.CASCADE,related_name='soal')
    jawaban = models.CharField(max_length=100, default='')
    file = models.FileField(upload_to='media/elearning/file',blank=True, null=True)
    def __str__(self):
        return '{}-{}'.format(self.student, self.jawaban)

class Nilai(models.Model):
    student_name = models.CharField(max_length=60, default='')
    student_id = models.CharField(max_length=20, default='')
    nilai = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return '{}-{}'.format(self.student_id, self.nilai)
