# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

# Create your models here.

class MataKuliah(models.Model):
    deskripsi = models.CharField(max_length=200)
    kode = models.CharField(max_length=10)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '{}-{}'.format(self.kode, self.deskripsi)

class Quiz(models.Model):
    mata_kuliah = models.ForeignKey('MataKuliah', on_delete=models.CASCADE,related_name='quiz')
    kode_kuis = models.CharField(max_length=10)

    def __str__(self):
        return '{}-{}'.format(self.mata_kuliah, self.kode_kuis)

class Soal(models.Model):
    quiz = models.ForeignKey('Quiz', on_delete=models.CASCADE,related_name='quiz')
    kode_quiz = models.CharField(max_length=20, default='MBSU_UTS_2018')
    created_date = models.DateTimeField(default=timezone.now)
    nomor = models.IntegerField(blank=True, null=True)
    pertanyaan = models.TextField()
    image = models.FileField(upload_to='media/elearning/img', blank=True, null=True)
    file = models.FileField(upload_to='media/elearning/file', blank=True, null=True)
    durasi_minute = models.IntegerField(default = 3)
    durasi_second = models.IntegerField(default = 0)
    def __str__(self):
        return '{}-{}'.format(self.quiz, self.nomor)

class Student(models.Model):
    created_date = models.DateTimeField(default=timezone.now)
    name = models.CharField(max_length=10)
    student_id =  models.CharField(max_length=10)
    def __str__(self):
        return '{}'.format(self.name)


class Answer(models.Model):
    student_name = models.CharField(max_length=50, default='')
    student_id = models.CharField(max_length=20, default='')
    soal = models.ForeignKey('Soal', on_delete=models.CASCADE,related_name='soal')
    jawab = models.TextField()
    file = models.FileField(upload_to='media/elearning/file',blank=True, null=True)
    def __str__(self):
        return '{}-{}'.format(self.student_name, self.student_id)

class Nilai(models.Model):
    student_name = models.CharField(max_length=60, default='')
    student_id = models.CharField(max_length=20, default='')
    nilai = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return '{}-{}'.format(self.student_id, self.nilai)
