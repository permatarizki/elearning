from django.conf.urls import url, include
from . import views

from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('api/soal', views.SoalViewSet)
router.register('api/answer', views.AnswerViewSet)

urlpatterns = [
    url(r'', include(router.urls)),
    url(r'^elearning/$',views.HomeView.as_view(),name='home'),
    url(r'^map/$',views.MapView.as_view(),name='map'),
    url(r'^uas/$',views.NilaiUASView.as_view(),name='uas'),
    url(r'^uas2019/$',views.NilaiUASView2.as_view(),name='uas'),
    url(r'^nilai/(?P<id>.+)$',views.NilaiByID.as_view(), name='nilai-filter'),
    # url(r'^soal/$',views.SoalView.as_view(),name='soal_list'),
    # url(r'^answer/$',views.CreateAnswerView.as_view(success_url="/answer/"), name='create_answer')
]
