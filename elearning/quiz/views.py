# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from django import forms

from django.shortcuts import render
from django.utils import timezone
from quiz.models import Soal, Answer, Nilai
from django.views.generic import (TemplateView,ListView,
                                  DetailView,CreateView,
                                  UpdateView,DeleteView)

from rest_framework import viewsets
from rest_framework import generics

from . import serializers

# Create your views here.
class AnswerViewSet(viewsets.ModelViewSet):
    """Handles CRUD for model Answer"""
    serializer_class = serializers.AnswerSerializer
    queryset = Answer.objects.all()

class SoalViewSet(viewsets.ModelViewSet):
    """Handles CRUD for model Soal"""
    serializer_class = serializers.SoalSerializer
    queryset = Soal.objects.all()


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ('student_name','student_id','soal', 'jawab','file', )

class CreateAnswerView(CreateView):
    redirect_field_name = '/answer/'
    form_class = AnswerForm
    model = Answer


class SoalView(ListView):
    model = Soal
    def get_queryset(self):
        return Soal.objects.filter(created_date__lte=timezone.now()).order_by('-created_date')
    # template_name = "home.html"
    # context_object_name = "all_soal"
	# def get_queryset(self):
	#     return list(Soal.objects.order_by('-created_date'))


class HomeView(TemplateView):
    template_name = 'home.html'

class MapView(TemplateView):
    template_name = 'map.html'

class NilaiUASView(TemplateView):
    template_name = 'uas.html'

class NilaiUASView2(TemplateView):
    template_name = 'uas2019.html'

class NilaiByID(generics.ListCreateAPIView):
    queryset = Nilai.objects.all()
    serializer_class = serializers.NilaiByIDSerializer

    def get_queryset(self):
        id = self.kwargs['id']
        return Nilai.objects.filter(student_id__iexact=id)
