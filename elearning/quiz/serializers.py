from rest_framework import serializers
from . import models


class SoalSerializer(serializers.ModelSerializer):
    """A serializer for Soal"""
    class Meta:
        model = models.Soal
        fields = '__all__'

class AnswerSerializer(serializers.ModelSerializer):
    """A serializer for Soal"""
    class Meta:
        model = models.Answer
        fields = '__all__'

class NilaiByIDSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Nilai
        fields = '__all__'
